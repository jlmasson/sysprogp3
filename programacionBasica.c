#include <stdio.h>
#include <string.h>

#define NUMPERSONAS 10

#define MAXCARACTERES 20

int verificarEdad(int edad, int validador);

int verificarCadena(char *);

int buscarValor(int *, int valor);

double calcularPromedio(int *edades);

int* obtenerMaxMin(int *);

// Validaciones funcionando
int main() {
    
    // Arreglos bidimensionales que permiten el manejo de la cantidad de nombres
    // y sus respectivos caracteres
    char nombres[NUMPERSONAS][MAXCARACTERES];
	char apellidos[NUMPERSONAS][MAXCARACTERES];
    
    // Arreglo unidimensional que contendrá las edades de las personas
	int edades[NUMPERSONAS];

	for (int i = 0; i < NUMPERSONAS; i++) {

		printf("Persona ");
		printf("%d\n", i + 1);

		char nombre[MAXCARACTERES];

		do {
			
			printf("Ingrese su nombre: ");
			scanf(" %s", nombre);
			getchar();

			if (verificarCadena(nombre) == 0 || verificarCadena(nombre) == -1) {
				printf("Nombre no válido.\n");
			}

		} while (verificarCadena(nombre) == 0 || verificarCadena(nombre) == -1);

		char apellido[MAXCARACTERES];

		do {
			
			printf("Ingrese su apellido: ");
			scanf(" %s", apellido);
			getchar();

			if (verificarCadena(apellido) == 0 || verificarCadena(apellido) == -1) {
				printf("Apellido no válido.\n");
			}

		} while (verificarCadena(apellido) == 0 || verificarCadena(apellido) == -1);

		int edad, validador;

		do {

			printf("Ingrese su edad: ");
		
			validador = scanf("\n%d", &edad);
			getchar();
            
            if (verificarEdad(edad, validador) == 0) {
				printf("Edad no válida.\n");
			}

		} while(verificarEdad(edad, validador) == 0);

		strcpy(nombres[i], nombre);
		strcpy(apellidos[i], apellido);
		edades[i] = edad;

		printf("\n");

	}

	double promedio;

	promedio = calcularPromedio(edades);

	int *maxMin = obtenerMaxMin(edades);

	int max, min;

	max = maxMin[0];
	min = maxMin[1];

	int indiceMinimo, indiceMaximo;

	indiceMinimo = buscarValor(edades, min);

	indiceMaximo = buscarValor(edades, max);

	if (indiceMinimo != -1 && indiceMaximo != -1) {
		printf("Promedio de edad = %.2f\n", promedio);
		printf("Persona mas vieja: %s %s, edad = %d\n", nombres[indiceMaximo],
			apellidos[indiceMaximo], max);
		printf("Persona mas joven: %s %s, edad = %d\n", nombres[indiceMinimo],
			apellidos[indiceMinimo], min);
	}

    return 0;
}

int verificarEdad(int edad, int validador) {
	if (validador == 1) {
		if (edad >= 0 && edad <= 130) {
			return 1;
		}
	}
	return 0;
}

int verificarCadena(char *cadena) {
	
	// -1 CADENA VACIA
	// 0 CADENA NO VALIDA
	// 1 CADENA VALIDA
	for (int i = 0; i < MAXCARACTERES; i++) {

		char c = cadena[i];

		// NULL
		if (c == 0) {
			break;
		}
		// VACIA
		if (c == 0 && i == 0) {
			return -1;
		}
		// PRIMERA LETRA MAYUSCULA
		else if ((c < 65 || c > 90) && i == 0) {
			return 0;
		}
		// DEMAS LETRAS MINISCULAS
		else if ((c < 97 || c > 122) && i > 0) {
			return 0;
		}
	}

	return 1;
}

int buscarValor(int *edades, int valor) {
	
	for (int i = 0; i < NUMPERSONAS; i++) {
		if (valor == edades[i]) {
			return i;
		}
	}
	return -1;

}

double calcularPromedio(int *edades) {
	
	double promedio;

	promedio = 0;

	for (int i = 0; i < NUMPERSONAS; i++) {
		promedio += edades[i];
	}
	
	return promedio / NUMPERSONAS;

}

// FUNCION QUE PERMITE OBTENER EL MAXIMO Y MINIMO DEL ARREGLO
int* obtenerMaxMin(int *edades) {
	static int maxMin[2];

	int max, min;

	max = 0;

	min = edades[0];

	for (int i = 0; i < NUMPERSONAS; i++) {
		if (edades[i] > max) {
			max = edades[i];
		}
		if (edades[i] < min) {
			min = edades[i];
		}
	}

	maxMin[0] = max;
	maxMin[1] = min;

	return maxMin;
}